# Imports
import simplematrixbotlib as matrix, os, yaml, requests, json

# Imports from
from dotenv import load_dotenv

# Open yaml configs - Commented at the moment because I have nothing to put in config.yaml
with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

# Load dotenv
load_dotenv()

# env and yaml variables
password = os.environ.get("PASSWORD")
homeserver = config['homeserver']

# Defind creds, assign a variable and prefix.
creds = matrix.Creds(homeserver, "freebot-fts", password)
client = matrix.Bot(creds)
PREFIX = '!'

# Misc commands
@client.listener.on_message_event
async def misc(room, message):
    match = matrix.MessageMatch(room, message, client, PREFIX)

    bot_help_message = f"""
    Help Message:
        prefix: !
        commands:
            help:
                command: help
                description: display help command
            ping:
                command: ping
                description: get the bot latency!
                """

    if match.is_not_from_this_bot() and match.prefix() and match.command("help"):
        await client.api.send_text_message(room.room_id, bot_help_message)

    if match.is_not_from_this_bot() and match.prefix() and match.command("ping"):
        seconds = requests.get(config['homeserver']).elapsed.total_seconds()
        await client.api.send_text_message(room.room_id, f"🏓 Pong! | Latency: {round(seconds * 1000)}ms")

# Run!
client.run()
